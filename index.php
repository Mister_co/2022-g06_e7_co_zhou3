<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Campus Order App</title>
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi"
      crossorigin="anonymous"
    />

    <link href="/style.css" rel="stylesheet" />
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3"
      crossorigin="anonymous"
    ></script>
  </head>
  <body>
    <div class="container">
      <div class="banner">
        <img src="/img/school.jpg" alt="Banner" />
      </div>

      <h1 class="display-6">
        Welcome to the Campus Order App
      </h1>
      <div class="row mt-2">
        <div class="col text-start">
            <a class="btn btn-primary" href="/login.php">Login</a>
        </div>
        <div class="col text-end">
            <a class="btn btn-primary" href="/register.php">Register</a>
        </div>
      </div>

      <div class="row mt-2">
        <img src="/img/appetizer.png" class="img-fluid col-sm-6" />
        <img src="/img/entree.png" class="img-fluid col-sm-6" />
        <img src="/img/dessert.png" class="img-fluid col-sm-6" />
        <img src="/img/drinks.png" class="img-fluid col-sm-6" />
      </div>
    </div>
  </body>
</html>
