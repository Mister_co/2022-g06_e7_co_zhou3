<?php
include_once "authenticate.php";
login();

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Campus Order App</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous" />

    <link href="/style.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
</head>

<body>
    <div class="container">
        <div class="banner">
            <img src="/img/school.jpg" alt="Banner" />
        </div>

        <div class="mt-2 text-end">
            <a href="/register.php" class="btn btn-primary">Registration</a>
        </div>

        <div class="mt-2 mb-4 row justify-content-center">
            <h1 class="col-auto display-6"><strong>Enter Username & Password</strong></h1>
        </div>
        <form method="post">
            <div class="mb-4 row justify-content-center">
                <div class="col-sm-6">
                    <label for="username" class="form-label">Your Username</label>
                    <input type="text" name="username" class="form-control" id="username" required />
                </div>
            </div>
            <div class="mb-4 row justify-content-center">
                <div class="col-sm-6">
                    <label for="password" class="form-label">Your Password</label>
                    <input type="password" name="password" class="form-control" id="password" required />
                </div>
            </div>

            <div class="text-center">
                <button type="submit" name="submit" class="btn btn-primary mb-3">Login</button>
            </div>
        </form>
    </div>
</body>

</html>