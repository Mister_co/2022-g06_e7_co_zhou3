<?php
include_once "authenticate.php";

checkLogin();
if (array_key_exists('logout', $_POST)) {
    session_destroy();
    header('Location: login.php');
}

$role = $_SESSION['role']
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Campus Order App</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous" />

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
    <link href="/style.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
</head>

<body>
    <div class="container">
        <div class="banner">
            <img src="/img/school.jpg" alt="Banner" />
        </div>
        <div class="row mt-2">
            <div class="col text-start">
                <a class="text-decoration-none" href="/index.php"><i class="icon bi bi-arrow-left text-dark"></i></a>
            </div>
            <div class="col text-end">
                <form method="post">
                    <input type="submit" name="logout" class="btn btn-primary" value="Logout" />
                </form>
            </div>
        </div>

        <div class="row mt-2">
            <a class="col-sm-6 text-center" href="/dishes.php">
                <img src="/img/appetizer.png" class="img-fluid dish-img" />
            </a>
            <a class="col-sm-6 text-center" href="/dishes.php">
                <img src="/img/entree.png" class="img-fluid dish-img" />
            </a>
            <a class="col-sm-6 text-center" href="/dishes.php">
                <img src="/img/dessert.png" class="img-fluid dish-img" />
            </a>
            <a class="col-sm-6 text-center" href="/dishes.php">
                <img src="/img/drinks.png" class="img-fluid dish-img" />
            </a>
        </div>

        <div class="row mt-2">
            <div class="col text-start">
                <?php
                if ($role === 'customer') {
                    echo '<span class="position-relative">
                        <a class="text-decoration-none" href="/order/cart.php"><i class="icon text-dark bi bi-cart"></i></a>
                        <span id="dish_count" class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                            0
                            <span class="visually-hidden">unread messages</span>
                        </span>
                    </span>';
                }

                if ($role === 'manager') {
                    echo '<a href="/manager_register.php" class="btn btn-primary">Register</a>';
                }
                ?>
            </div>
            <div class="col text-end">
                <?php
                if ($role === 'customer') {
                    echo '<a class="text-decoration-none" href="/order/history.php">
                    <i class="icon bi bi-clock-history text-dark">
                    </i>
                    </a>';
                }

                if ($role === 'manager') {
                    echo '<a href="/manager_register.php"
                     class="btn btn-primary">Checking Order</a>';
                }
                ?>

            </div>
        </div>
    </div>
</body>

</html>