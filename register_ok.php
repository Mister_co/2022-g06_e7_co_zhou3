<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Campus Order App</title>
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi"
      crossorigin="anonymous"
    />

    <link href="/style.css" rel="stylesheet" />
  </head>
  <body>
    <div class="container">
      <div class="banner">
        <img src="/img/school.jpg" alt="Banner" />
      </div>

      <h1 class="display-4 text-center">Register successully!</h1>
      <div class="text-center"><a href="/login.php" class="mt-4 btn btn-primary mb-3">Login</a></div>
    </div>
  </body>
</html>
