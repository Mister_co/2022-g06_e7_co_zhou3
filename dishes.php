<?php
include_once 'config.php';
// Prepare our SQL, preparing the SQL statement will prevent SQL injection.
$result = $con->query('SELECT * from dishes');
$dishes = array();
while ($row = $result->fetch_object()) {
    array_push($dishes, $row);
};

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Campus Order App</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous" />

    <link href="/style.css" rel="stylesheet" />

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
</head>

<body>
    <div class="container">
        <div class="banner">
            <img src="/img/school.jpg" alt="Banner" />
        </div>
        <div class="mt-2 text-start">
            <a class="text-decoration-none" href="/home.php"><i class="icon bi bi-arrow-left text-dark"></i></a>
        </div>

        <div class="row mt-2 justify-content-between">
            <?php
            foreach ($dishes as $dish) {

                echo '<div class="col-md-6 border-dark row position-relative">' .
                    '<img src="/img/appetizer.png" class="img-fluid col-4"></img>
                <div class="col-8 text-break">
                  <strong class="display-7">' .
                    $dish->name .
                    '</strong>
                  <p>' .
                    $dish->description . '
                  </p>
                  <p>€' . $dish->price . '
                  </p>
                </div>
                <div class="position-absolute">
                <i class="icon dish-op bi bi-plus-circle"></i>
                      </div>
              </div>';
            };
            ?>
        </div>

        <div class="row mt-2">
            <div class="col text-start">
                <span class="position-relative">
                    <a class="text-decoration-none" href="/order/cart.php"><i class="icon text-dark bi bi-cart"></i></a>
                    <span id="dish_count" class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                        0
                        <span class="visually-hidden">unread messages</span>
                    </span>
                </span>
            </div>
            <div class="col text-end">
                <a class="text-decoration-none" href="/order/history.php">
                    <i class="icon bi bi-clock-history text-dark">
                    </i>
                </a>

            </div>
        </div>
    </div>
</body>

</html>