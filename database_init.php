<?php
$create_db = "CREATE DATABASE IF NOT EXISTS " . $db_name . ";";
$use_db = "USE " . $db_name . ";";

// $drop_all = "SET FOREIGN_KEY_CHECKS = 0;
// drop table if exists `users`;
// drop table if exists `orders`;
// drop table if exists `dishes`;
// SET FOREIGN_KEY_CHECKS = 1;";

$create_user_table = "CREATE TABLE IF NOT EXISTS `users` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `username` varchar(25) NOT NULL,
    `password` varchar(255) NOT NULL,
    `role` varchar(25) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;";

$create_order_table = "CREATE TABLE IF NOT EXISTS `orders` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `date_created` DATE NOT NULL,
    `status` varchar(25) NOT NULL,
    `finalcost` FLOAT NOT NULL,
    `creator_id` int(10) unsigned NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`creator_id`) REFERENCES users(`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;";

$create_dish_table = "CREATE TABLE IF NOT EXISTS `dishes` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(25) NOT NULL,
    `type` varchar(25) NOT NULL,
    `description` varchar(255),
    `price` FLOAT NOT NULL,
    `imgUrl` varchar(255),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;";
$user_insert = "INSERT IGNORE INTO users (username, password, role) VALUES ('adam', '123', 'customer');
                INSERT IGNORE INTO users (username, password, role) VALUES ('bob', '1234', 'manager');";

$dish_insert = "INSERT INTO dishes (name, description, type, price)
                SELECT * FROM (SELECT 'Steak', 'steak,egg and vegie', 'Entree', 18.99) AS tmp
                WHERE NOT EXISTS (
                   SELECT name FROM dishes WHERE name = 'Steak'
                ) LIMIT 1;
                INSERT INTO dishes (name, description, type, price)
                SELECT * FROM (SELECT 'Pepsi', 'Soda Drink', 'Drink', 2.44) AS tmp
                WHERE NOT EXISTS (
                   SELECT name FROM dishes WHERE name = 'Pepsi'
                ) LIMIT 1;
                INSERT INTO dishes (name, description, type, price)
                SELECT * FROM (SELECT 'Ice Cream', 'delicious ice cream', 'Dessert', 4.5) AS tmp
                WHERE NOT EXISTS (
                   SELECT name FROM dishes WHERE name = 'Ice Cream'
                ) LIMIT 1;
                INSERT INTO dishes (name, description, type, price)
                SELECT * FROM (SELECT 'Chicken Soup', 'chicken soup description', 'Plat', 7.89) AS tmp
                WHERE NOT EXISTS (
                   SELECT name FROM dishes WHERE name = 'Chicken Soup'
                ) LIMIT 1;";


$sql = $create_db . $use_db . $create_user_table . $create_order_table . $create_dish_table . $user_insert . $dish_insert;

$con->multi_query($sql);
do {
    // if ($result = $con->store_result()) {
    //     while ($row = $result->fetch_row()) {
    //         printf("%s\n", $row[0]);
    //     }
    // }
    $result = $con->store_result();
} while ($con->next_result());
