## installation
Use XAMPP to install php and mysql if not already. It can also help host a local server
* XAMPP: https://www.apachefriends.org/

* after download, please put all files in the the directory Path/to/xampp/htdocs
* then start Apache start and go to http://localhost

### Default users for testings
#### User #1
* username: adam
* password: 123
* role: customer
#### User #2
* username: bob
* password: 1234
* role: manager

Notes: You can custom your user in the database_init.php (modify the sql queries)