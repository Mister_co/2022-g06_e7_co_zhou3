<?php
include_once 'authenticate.php';

checkRole("manager");
include_once 'config.php';

$role = 'manager';
$user_exist = FALSE;
if (isset($_POST['submit'])) {
  // check if user exist
  $result = $con->query("SELECT * FROM users WHERE username='" . $_POST['username'] . "'");
  $num_rows = $result->num_rows;
  $result->free_result();
  if ($num_rows) {
    $user_exist = TRUE;
  } else {
    $query = 'INSERT INTO users (username, password, role) VALUES (?, ?, ?)';
    // Prepare our SQL, preparing the SQL statement will prevent SQL injection.
    if ($stmt = $con->prepare($query)) {
      // Bind parameters (s = string, i = int, b = blob, etc), in our case the username is a string so we use "s"
      $stmt->bind_param('sss', $_POST['username'], $_POST['password'], $role);
      $stmt->execute();
      // Store the result so we can check if the account exists in the database.
      $stmt->store_result();
      if ($stmt->affected_rows > 0) {
        header('Location: register_ok.php');
      }


      $stmt->close();
    }
  }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Campus Order App</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous" />

  <link href="/style.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
</head>

<body>
  <div class="register-wrapper">
    <div class="register-box container">
      <div class="row justify-content-center"><img src="/img/school_logo.png" class="img-fluid" alt="logo" /></div>

      <?php if ($user_exist) {
        echo '<div class="alert alert-danger col-sm-10 m-auto" role="alert">
          User Already registered
        </div>';
      } ?>

      <form method="post">
        <div class="mb-4 row justify-content-center">
          <div class="col-sm-10">
            <label for="username" class="form-label">Manager Username</label>
            <input required type="text" name="username" class="form-control" id="username" />
          </div>
        </div>
        <div class="mb-4 row justify-content-center">
          <div class="col-sm-10">
            <label for="password" class="form-label">Password</label>
            <input required type="password" name="password" class="form-control" id="password" />
          </div>
        </div>
        <div class="mb-4 row justify-content-center">
          <div class="col-sm-10">
            <label for="confirm" class="form-label">Password Confirm</label>
            <input required type="password" class="form-control" name="confirm" id="confirm" />
          </div>
        </div>

        <div class="text-center mb-2">
          <button type="submit" name="submit" class="btn btn-primary">Register</button>
        </div>
      </form>
      <div class="text-center">
        <a href="/login.php" class="link-primary">Back to Login</a>
      </div>
    </div>
  </div>
</body>

</html>