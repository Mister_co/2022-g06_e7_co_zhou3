<?php
function login()
{
    include_once 'config.php';
    if (isset($_POST['submit'])) {
        // Prepare our SQL, preparing the SQL statement will prevent SQL injection.
        if ($stmt = $con->prepare('SELECT id, password, role FROM users WHERE username = ?')) {
            // Bind parameters (s = string, i = int, b = blob, etc), in our case the username is a string so we use "s"
            $stmt->bind_param('s', $_POST['username']);
            $stmt->execute();
            // Store the result so we can check if the account exists in the database.
            $stmt->store_result();

            if ($stmt->num_rows > 0) {
                $stmt->bind_result($id, $password, $role);
                $stmt->fetch();
                // Account exists, now we verify the password.
                // Note: remember to use password_hash in your registration file to store the hashed passwords.
                if (($_POST['password'] === $password)) {
                    // Verification success! User has logged-in!
                    // Create sessions, so we know the user is logged in, they basically act like cookies but remember the data on the server.
                    session_start();
                    session_regenerate_id();
                    $_SESSION['loggedin'] = TRUE;
                    $_SESSION['username'] = $_POST['username'];
                    $_SESSION['role'] = $role;
                    $_SESSION['id'] = $id;

                    header('Location: home.php');
                } else {
                    echo "<script>alert('Incorrect password!');</script>";
                }
            } else {
                // Incorrect username
                echo "<script>alert('username not exist');</script>";
            }


            $stmt->close();
        }
    }
}

// function register($role)
// {
//     include_once 'config.php';

//     if (isset($_POST['submit'])) {
//         // check if user exist
//         $result = $con->query("SELECT * FROM users WHERE username='" . $_POST['username'] . "'");
//         $num_rows = $result->num_rows;
//         $result->free_result();
//         if ($num_rows) {
//             $user_exist = TRUE;
//             return;
//         } else {
//             $query = 'INSERT INTO users (username, password, role) VALUES (?, ?, ?)';
//             // Prepare our SQL, preparing the SQL statement will prevent SQL injection.
//             if ($stmt = $con->prepare($query)) {
//                 // Bind parameters (s = string, i = int, b = blob, etc), in our case the username is a string so we use "s"
//                 $stmt->bind_param('sss', $_POST['username'], $_POST['password'], $role);
//                 $stmt->execute();
//                 // Store the result so we can check if the account exists in the database.
//                 $stmt->store_result();
//                 if ($stmt->affected_rows > 0) {
//                     header('Location: register_ok.php');
//                 }


//                 $stmt->close();
//             }
//         }
//     }
// }


function checkLogin() {
    session_start();

    if (!isset($_SESSION) || !$_SESSION['role'] ) {
        header('Location: login.php');
    }

}

function checkRole($role) {
    session_start();

    if (!isset($_SESSION) || $_SESSION['role'] !== $role ) {
        header('Location: login.php');
    }

}